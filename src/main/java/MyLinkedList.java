/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kenny
 */
public class MyLinkedList<T> {
    
    private Node<T> first = null;
	
    /**
     * Insert at the front of the list
     * @param node
     */
    public void insert(Node<T> node) {
            node.setNext(first);
            first = node;
    }

    /**
     * Remove from the front of the list
     * @param node
     */
    public void remove(){
            if(first.getNext()!=null)
                first = first.getNext();
            else first = null;
    }

    /**
     * Recursively traverse this list and print the node value
     * @param node
     */
    private void printList(Node<T> node) {
            System.out.println("Node is " + node.getValue());
            if(node.getNext()!=null) printList(node.getNext());
    }

    public void print(){
            printList(first);
    }
}
