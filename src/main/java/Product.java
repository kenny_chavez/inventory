
import java.util.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kenny
 */
public class Product {
    private String code;
    private String description;
    private float cost;
    private float price;
    private String category;
    private String warehouse;
    private String hallway;
    private String shelving;
    private int stock;
    private Date warehouseEntry;
    private boolean isDevolution;
    
    public Product(String code, String description, float cost, float price, String category, String warehouse, String hallway,
                String shelving, int stock, Date warehouseEntry, boolean isDevolution) {
        this.code = code;
        this.description = description;
        this.cost = cost;
        this.price = price;
        this.category = category;
        this.warehouse = warehouse;
        this.hallway = hallway;
        this.shelving = shelving;
        this.stock = stock;
        this.warehouseEntry = warehouseEntry;
        this.isDevolution = isDevolution;
    }

    @Override
    public String toString(){
        return "Product { code: " + this.getCode()
            + "\nDescription: " + this.getDescription()
            + "\nCategory: " + this.getCategory()
            + "\nCost: " + this.getCost()
            + "\nPrice: " + this.getPrice()
            + "\nWarehouse: " + this.warehouse
            + "\nHallway: " + this.hallway
            + "\nShelving: " + this.shelving
            + "\nStock: " + this.stock
            + "\nWarehouseEntry: " + this.warehouseEntry
            + "\nIsDevolution: " + this.isDevolution
            + " }";
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the cost
     */
    public float getCost() {
        return cost;
    }

    /**
     * @param cost the cost to set
     */
    public void setCost(float cost) {
        this.cost = cost;
    }

    /**
     * @return the price
     */
    public float getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(float price) {
        this.price = price;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return the warehouse
     */
    public String getWarehouse() {
        return warehouse;
    }

    /**
     * @param warehouse the warehouse to set
     */
    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    /**
     * @return the hallway
     */
    public String getHallway() {
        return hallway;
    }

    /**
     * @param hallway the hallway to set
     */
    public void setHallway(String hallway) {
        this.hallway = hallway;
    }

    /**
     * @return the shelving
     */
    public String getShelving() {
        return shelving;
    }

    /**
     * @param shelving the shelving to set
     */
    public void setShelving(String shelving) {
        this.shelving = shelving;
    }

    /**
     * @return the stock
     */
    public int getStock() {
        return stock;
    }

    /**
     * @param stock the stock to set
     */
    public void setStock(int stock) {
        this.stock = stock;
    }

    /**
     * @return the warehouseEntry
     */
    public Date getWarehouseEntry() {
        return warehouseEntry;
    }

    /**
     * @param warehouseEntry the warehouseEntry to set
     */
    public void setWarehouseEntry(Date warehouseEntry) {
        this.warehouseEntry = warehouseEntry;
    }
    
    /**
     * @return the devolution
     */
    public boolean getIsDevolution() {
        return isDevolution;
    }

    /**
     * @param warehouseEntry the warehouseEntry to set
     */
    public void setIsDevolution(boolean isDevolution) {
        this.isDevolution = isDevolution;
    }
    
}
