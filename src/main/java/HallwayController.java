
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kenny
 */
public class HallwayController {
    MyLinkedList<Hallway> list = new MyLinkedList<Hallway>();
    ArrayList<Hallway> report = new ArrayList<Hallway>();
    MainController main = MainController.getInstance();
    
    public void add(Hallway hallway){
        list.insert(new Node<Hallway>(hallway));
        list.print();
        report.add(hallway);
        main.saveHallway(hallway);
    }
    
    public void createReport(){
        String path = "/home/kenny/Documents/University/reports/";
        String filename = "hallway-report.csv";
        
        try {
            PrintWriter pw = new PrintWriter(new File(path + filename));
            StringBuilder sb = new StringBuilder();
            
            sb.append("Código, Bodega, Descripción\r\n");
            report.forEach(hallway -> {
                sb.append(hallway.getCode() + "," + hallway.getWarehouseCode()+ "," + hallway.getDescription() + "\r\n");
            });
            
            pw.write(sb.toString());
            
            System.out.println("************************************************************************************");
            System.out.println("*SE HA GENERADO EL ARCHIVO EN: " + path + "*");
            System.out.println("************************************************************************************");
            pw.close();
        } catch(Exception e) {
            System.out.println("Error al cargar el archivo"+e);
        }
    }
}
