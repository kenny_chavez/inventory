/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kenny
 */
public class Shelving {
    private String code;
    private String warehouseCode;
    private String hallwayCode;
    private String description;
    
    public Shelving(String code, String warehouseCode, String hallwayCode, String description) {
        this.code = code;
        this.hallwayCode = hallwayCode;
        this.description = description;
        this.warehouseCode = warehouseCode;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the warehouseCode
     */
    public String getWarehouseCode() {
        return warehouseCode;
    }

    /**
     * @param warehouseCode the warehouseCode to set
     */
    public void setWarehouseCode(String warehouseCode) {
        this.warehouseCode = warehouseCode;
    }

    /**
     * @return the hallwayCode
     */
    public String getHallwayCode() {
        return hallwayCode;
    }

    /**
     * @param hallwayCode the hallwayCode to set
     */
    public void setHallwayCode(String hallwayCode) {
        this.hallwayCode = hallwayCode;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    @Override
    public String toString(){
        return "Shelving { code: " + this.code 
            + ", \ndescription: " + this.description 
            + "\nWarehouseCode: " + this.warehouseCode
            + "\nHallwayCode: " + this.hallwayCode + " }";
    }
}
