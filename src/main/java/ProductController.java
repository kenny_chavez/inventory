
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kenny
 */
public class ProductController {
    MyLinkedList<Product> list = new MyLinkedList<Product>();
    ArrayList<Product> report = new ArrayList<Product>();
    MainController main = MainController.getInstance();
    
    public void add(Product product){
        list.insert(new Node<Product>(product));
        list.print();
        report.add(product);
        main.saveProduct(product);
    }
    
    public void createReport(){
        String path = "/home/kenny/Documents/University/reports/";
        String filename = "product-report.csv";
        
        try {
            PrintWriter pw = new PrintWriter(new File(path + filename));
            StringBuilder sb = new StringBuilder();
            
            sb.append("Código, Descripción, Categoria, Cantidad, Costo, Precio, Bodega, Pasillo, Estanteria, Devolución, Fecha de Ingreso\r\n");
            report.forEach(product -> {
                sb.append(product.getCode() + "," + product.getDescription() + "," 
                    + product.getCategory() + "," + product.getStock() + ", "  
                    + product.getCost() + "," + product.getPrice() + ","
                    + product.getWarehouse() + ", " + product.getHallway() + ","
                    + product.getShelving() + "," + product.getIsDevolution() + "," + product.getWarehouseEntry() + "\r\n");
            });
            
            pw.write(sb.toString());
            
            System.out.println("************************************************************************************");
            System.out.println("*SE HA GENERADO EL ARCHIVO EN: " + path + "*");
            System.out.println("************************************************************************************");
            pw.close();
        } catch(Exception e) {
            System.out.println("Error al cargar el archivo"+e);
        }
    }
}
