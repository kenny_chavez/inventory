
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kenny
 */
public class WarehouseController {
    MyLinkedList<Warehouse> list = new MyLinkedList<Warehouse>();
    ArrayList<Warehouse> report = new ArrayList<Warehouse>();
    MainController main = MainController.getInstance();
    
    public void add(Warehouse warehouse){
        list.insert(new Node<Warehouse>(warehouse));
        list.print();
        report.add(warehouse);
        main.saveMainWarehouse(warehouse);
    }
    
    public void createReport(){
        String path = "/home/kenny/Documents/University/reports/";
        String filename = "warehouse-report.csv";
        
        try {
            PrintWriter pw = new PrintWriter(new File(path + filename));
            StringBuilder sb = new StringBuilder();
            
            sb.append("Código, Dirección, Descripción \r\n");
            report.forEach(warehouse -> {
                sb.append(warehouse.getCode() + "," + warehouse.getAddress() + "," + warehouse.getDescription() + "\r\n");
            });
            
            pw.write(sb.toString());
            
            System.out.println("************************************************************************************");
            System.out.println("*SE HA GENERADO EL ARCHIVO EN: " + path + "*");
            System.out.println("************************************************************************************");
            pw.close();
        } catch(Exception e) {
            System.out.println("Error al cargar el archivo"+e);
        }
    }
}
