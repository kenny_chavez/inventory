/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kenny
 */
public class Hallway {

    private String code;
    private String description;
    private String warehouseCode;

    public Hallway(String code, String description, String warehouseCode) {
        this.code = code;
        this.description = description;
        this.warehouseCode = warehouseCode;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the warehouseCode
     */
    public String getWarehouseCode() {
        return warehouseCode;
    }

    /**
     * @param warehouseCode the warehouseCode to set
     */
    public void setWarehouseCode(String warehouseCode) {
        this.warehouseCode = warehouseCode;
    }
    
    @Override
    public String toString(){
        return "Hallway { code: " + this.code 
            + ", \ndescription: " + this.description 
            + "\nwarehouseCode: " + this.warehouseCode + " }";
    }

}
