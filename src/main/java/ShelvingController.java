
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kenny
 */
public class ShelvingController {
    MyLinkedList<Shelving> list = new MyLinkedList<Shelving>();
    ArrayList<Shelving> report = new ArrayList<Shelving>();
    MainController main = MainController.getInstance();
    
    public void add(Shelving shelving){
        list.insert(new Node<Shelving>(shelving));
        list.print();
        report.add(shelving);
        main.saveShelving(shelving);
    }
    
    public void createReport(){
        String path = "/home/kenny/Documents/University/reports/";
        String filename = "shelving-report.csv";
        
        try {
            PrintWriter pw = new PrintWriter(new File(path + filename));
            StringBuilder sb = new StringBuilder();
            
            sb.append("Código, Bodega, Pasillo, Descripción\r\n");
            report.forEach(shelving -> {
                sb.append(shelving.getCode() + "," + shelving.getWarehouseCode()+ "," + shelving.getHallwayCode() + "," + shelving.getDescription() + "\r\n");
            });
            
            pw.write(sb.toString());
            
            System.out.println("************************************************************************************");
            System.out.println("*SE HA GENERADO EL ARCHIVO EN: " + path + "*");
            System.out.println("************************************************************************************");
            pw.close();
        } catch(Exception e) {
            System.out.println("Error al cargar el archivo"+e);
        }
    }
}
