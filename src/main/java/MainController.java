
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kenny
 */
public class MainController {
    
    private static MainController mainInstance = null;
    ArrayList<Warehouse> whList;
    ArrayList<Hallway> hwList;
    ArrayList<Shelving> sList;
    ArrayList<Product> pList;
    
    private MainController(){
        whList = new ArrayList<Warehouse>();
        hwList = new ArrayList<Hallway>();
        sList = new ArrayList<Shelving>();
        pList = new ArrayList<Product>();
    }
    
    public static MainController getInstance() {
        if(mainInstance == null)
            mainInstance = new MainController();
        
        return mainInstance;
    }
    
    public void saveMainWarehouse(Warehouse warehouse){
        whList.add(warehouse);
    }
    
    public ArrayList<Warehouse> getWarehouseList(){
        return whList;
    }
    
    public void saveHallway(Hallway hallway){
        hwList.add(hallway);
    }
    
    public ArrayList<Hallway> getHallwayList(){
        return hwList;
    }
    
    public void saveShelving(Shelving shelving) {
        sList.add(shelving);
    }
    
    public ArrayList<Shelving> getShelvingList(){
        return sList;
    }
    
    public ArrayList<String> findHallwayByCode(String warehouseCode){
        ArrayList<String> hallwayFind = new ArrayList<String>();
        
        hwList.forEach(hallway -> {
            if(hallway.getWarehouseCode().equals(warehouseCode)){
                hallwayFind.add(hallway.getCode());
            }
        });
        
        return hallwayFind;
    }
    
    public void saveProduct(Product product) {
        pList.add(product);
    }
    
    public ArrayList<Product> getProductList(){
        return pList;
    }
    
    public ArrayList<String> findShelvingByCode(String hallwayCode){
        ArrayList<String> shelvingFind = new ArrayList<String>();
        
        sList.forEach(shelving -> {
            if(shelving.getHallwayCode().equals(hallwayCode)){
                shelvingFind.add(shelving.getCode());
            }
        });
        
        return shelvingFind;
    }
}
