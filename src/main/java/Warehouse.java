/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kenny
 */
public class Warehouse {
    private String code;
    private String address;
    private String description;
    
    public Warehouse(String code, String address, String description){
        this.address = address;
        this.description = description;
        this.code = code;
    }
    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }
    
    @Override
    public String toString(){
        return "Warehouse { code: " + this.code 
            + ", \ndescription: " + this.description 
            + "\naddress: " + this.address + " }";
    }
    
}
